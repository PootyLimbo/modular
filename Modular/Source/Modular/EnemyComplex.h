// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Room.h"
#include "EnemyComplex.generated.h"

UCLASS()
class MODULAR_API AEnemyComplex : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemyComplex();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

public:
	UFUNCTION(BlueprintCallable, Category = EnemyComplex)
	void createRoom();

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Rooms)
	TArray<ARoom*> rooms;
	
};
