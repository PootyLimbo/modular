// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Room.generated.h"

class UInstancedStaticMeshComponent;

USTRUCT(BlueprintType)
struct FTileData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Tiles)
	AActor* tile = nullptr;

};

UCLASS()
class MODULAR_API ARoom : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARoom();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(BlueprintCallable, Category = TileSystem)
	void createRoom();

public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = TileSystem)
	int32 noOfUniqueTiles = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = TileSystem)
	TArray<UInstancedStaticMeshComponent*> InstancedComponent;
	
	// Variables for tile logic
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = TileSystem)
	float tileSizeX = 96.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = TileSystem)
	float tileSizeY = 96.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = TileSystem)
	FVector tileSpacing = FVector(0.f, 96.f, 96.f);

	// Size of the room in number of tiles
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = TileSystem)
	uint8 roomWidth = 2;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = TileSystem)
	uint8 roomHeight = 2;

	// Array of actors used to generate the room's structure (tile-based)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = TileSystem)
	TArray<FTileData> tiles;
	
};
