// Fill out your copyright notice in the Description page of Project Settings.

#include "Modular.h"
#include "Room.h"
#include "Components/InstancedStaticMeshComponent.h"

// Sets default values
ARoom::ARoom()
{
	createRoom();
}

// Called when the game starts or when spawned
void ARoom::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARoom::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ARoom::createRoom()
{
	for (int32 i = 0; i < noOfUniqueTiles; i++)
	{
		FName n = "TileSet" + i;
		PrimaryActorTick.bCanEverTick = true;
		UInstancedStaticMeshComponent* newISMC;
		newISMC = CreateDefaultSubobject<UInstancedStaticMeshComponent>(n);
		newISMC->AddInstance(FTransform(FVector(0, 0, 0)));
		newISMC->AttachTo(RootComponent);
		InstancedComponent.Add(newISMC);
		//InstancedComponent->AddInstance();
	}

	for (int32 i = 0; i < InstancedComponent.Num(); i++)
	{
		InstancedComponent[i]->UpdateInstanceTransform(0, FTransform(FVector(0, 0, i * 100)));
	}
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	//PrimaryActorTick.bCanEverTick = true;
}